let imageIndex = 0;

export function hero() {
  const element = document.getElementById('heroParent');
  if (!element) {
    return;
  }

  setInterval(() => changeWallpaper(element), 5000);
}

const path = '../../assets/wallpapers/';
const imageSrcs = [
  'Abstract.opti.webp',
  'Animated.opti.webp',
  'Aurora.opti.webp',
  'Aurora Dragon.opti.webp',
  'Circuit.opti.webp',
  'Colors.opti.webp',
  'Dragon.opti.webp',
  'Ghosts.opti.webp',
  'Imperial Eagle.opti.webp',
  'Malefor.opti.webp',
  'Orange Glow.opti.webp',
  'Raptor.opti.webp',
  'Totem.opti.webp',
  'Talon.opti.webp',
  'Wave.opti.webp',
  'White Tailed Eagle.opti.webp',
  'hawk-bg.opti.webp',
];

function changeWallpaper(element: HTMLElement) {
  const imageSrc = path + imageSrcs[imageIndex];

  element.style.backgroundImage = `url(${imageSrc})`;
  imageIndex = (imageIndex + 1) % imageSrcs.length;
}
