import { Routes } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { DownloadComponent } from './download/download.component';

export const routes: Routes = [
    {
        path: '',
        component: LandingComponent,
        title: 'Garuda Linux - Powerful yet easy',
    },
    {
        path: 'download',
        component: DownloadComponent,
        title: 'Garuda Linux | Download',
    },
];
