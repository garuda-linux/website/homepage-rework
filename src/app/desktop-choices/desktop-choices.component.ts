import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-desktop-choices',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './desktop-choices.component.html',
  styleUrl: './desktop-choices.component.scss'
})
export class DesktopChoicesComponent {

}
