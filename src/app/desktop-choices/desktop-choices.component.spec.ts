import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesktopChoicesComponent } from './desktop-choices.component';

describe('DesktopChoicesComponent', () => {
  let component: DesktopChoicesComponent;
  let fixture: ComponentFixture<DesktopChoicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DesktopChoicesComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DesktopChoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
