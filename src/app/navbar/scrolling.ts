window.addEventListener('scroll', transparentScrolling)

console.log('test')

function transparentScrolling() {

    const navbar = document.getElementById('navbarParent');
    if (!navbar) {
        return
    }

    const viewportHeight = window.innerHeight;
    if (document.documentElement.scrollTop > viewportHeight) {
      navbar.classList.add("navbarParentTransparent");
    }

    navbar.classList.remove("navbarParentTransparent");
}