import { CommonModule, ViewportScroller } from '@angular/common';
import { AfterViewInit, Component, ElementRef, HostListener, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [RouterModule, CommonModule],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss',
})
export class NavbarComponent {
  navbar: boolean = true;
  transparent: boolean = false;
  blacklisted_routes: string[] = ['download'];
  transparent_routes: string[] = ['/'];

  constructor(
    private elementRef: ElementRef,
    router: Router,
  ) {
    router.events.subscribe((event) => {
      this.toggleRendering(router);
      this.toggleTransparency(router);
    });
  }

  private toggleRendering(router: Router) {
    if (this.blacklisted_routes.includes(router.url)) {
      this.navbar = false;
      return;
    }

    this.navbar = true;
  }

  private toggleTransparency(router: Router) {
    if (this.transparent_routes.includes(router.url)) {
      this.transparent = true;
      return;
    }

    this.transparent = false;
  }

  @HostListener('document:scroll', ['$event'])
  public onViewportScroll(router: Router) {

    if (!this.transparent_routes.includes(router.url)) {
      return;
    }

    console.log('scroll')

    const navbar = document.getElementById('navbarParent');
    if (!navbar) {
      return;
    }

    if (document.documentElement.scrollTop > 0) {
      navbar.classList.remove('navbarParentTransparent');
    } else {
      navbar.classList.add('navbarParentTransparent');
    }

    const downloadButtonText = document.getElementById('navbarDownloadButtonText')

    if (downloadButtonText) {

      const viewportHeight = window.innerHeight;
      if (document.documentElement.scrollTop > viewportHeight) {
        downloadButtonText.classList.remove('hidden');
      }
      else {
        downloadButtonText.classList.add('hidden');
      }
    }
  }
}