import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.scss'
})
export class FooterComponent {
  footer: boolean = false;
  blacklisted_routes: string[] = [];

  constructor(router: Router) {
    router.events.subscribe((event) => {

      if (this.blacklisted_routes.includes(router.url)) {
        this.footer = false;
        return;
      }

      this.footer = true;
    });
  }
}
