import { sleep } from "./utils"

export enum AnimationName {
    FadeToLeft = "fadeToLeft",
}

export async function animateElement(element: HTMLElement, animationName: AnimationName, duration = 1000, delay = 0) {

    await sleep(delay)

    let secondsStr = (duration / 1000) + "s"
    element.style.animation = `${animationName} ${secondsStr}`

    console.log('animating ' + animationName, 'for duration ' + duration)

    await sleep(duration)

    console.log('killing')

    element.remove()
}