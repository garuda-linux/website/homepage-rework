// Licensed under MIT by Carson Burke @MarvinTMB

// Not exactly the most efficient parallax implementation, but it should work fine enough
// Use (css) classes parallaxify for <img /> tags, and parallaxifyBg for background images

export function parallax() {
  let imagesToParallax = Array.from(
    document.getElementsByClassName('parallaxifyBg'),
  );

  for (let element of imagesToParallax) {
    if (element instanceof HTMLElement === false) {
      continue;
    }

    if (!element.dataset['speedMultiplier']) {
      element.dataset['speedMultiplier'] = '3';
    }

    element.style.backgroundAttachment = 'fixed';
    element.style.backgroundPositionY =
      roundTo(
        getPosition(element) * parseInt(element.dataset['speedMultiplier']),
        2,
      ) + 'px';
  }
}

function getPosition(element: HTMLElement) {
  let top = (element.getBoundingClientRect().top / window.innerHeight) * 100;

  return top;
}

function roundTo(number: number, decimals: number) {
  return parseInt(number.toFixed(decimals));
}
