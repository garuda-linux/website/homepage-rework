import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import { parallax } from './parallaxify';
import { hero } from './hero';

bootstrapApplication(AppComponent, appConfig).catch((err) =>
  console.error(err),
);

window.addEventListener('load', parallax);
window.addEventListener('scroll', parallax);

window.addEventListener('load', hero)