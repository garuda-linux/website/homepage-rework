# Structure

Please make issues regarding bug reports and feature requests.

Please create merge requests to commit code to the project.

# Formatting

Please follow existing formatting rules with `prettier` and `eslint`

# CSS

For modifications to CSS, please use variables wherever possible, and limit the use of global classes.

Note that all default styling for tags is overwritten. For example, `h1` tags have no different font size or margin than any other unstyled text in the app.

## Semantic vs styling

There should be a clear differentiation between styling and semantic HTML. For example, a `h1` tag may have a `class="h3"` where the tag name describes the semantic value while the class describes the font size and weight. This is for accessibility, organizational and SEO purposes.

## Style classes not tags

Wherever possible, avoid default stylings for tags, children, etc. and instead, style based on classes. For example, do not style a table's `<tr></tr>` using the parent's class `.tableClass { tr { [styleAttributes] }}` but instead assign a class to the `tr`'s and style it through that.

This is unavoidable in some cases, such as making head and script tags invisible

# HTML

Wherever possible, HTML should be in html files with components linking to them using the templateUrl property.

## Semantic

The hierarchy and value of some tags should be respected for accessibility and SEO purposes. Examples: `h1` tags are for the most important text(s) on the page, `section`s are for each distinct section of content, `main` all content disregarding `head`, `script`, `nav` and `footer` content, etc. 